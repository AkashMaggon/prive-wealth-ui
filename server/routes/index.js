
var forum = require('./forum')
var express = require('express')
const router = express.Router();
const hbs = require('hbs');

router.get('/health-check', (req, res) => {
  res.send('OK');
})

router.get('/', (req, res) =>{
 res.render('bayfront.hbs');


})

router.use('/forum',forum);



module.exports =router;