var express = require('express')
const router = express.Router();
var forumCtrl = require('../controllers/forums')

router.route('/').get(forumCtrl.getForums);

//router.route('/createForum').get(forumCtrl.createForum);


module.exports = router;