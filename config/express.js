var express = require('express');
const app = express();
var routes = require('../server/routes/index')
var bodyParser = require('body-parser')

const hbs = require('hbs');


app.set('viewengine','hbs');

hbs.registerPartials('C:/Users/Akash/Documents/Git/prive-wealth-ui/views/partials')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));

app.use(express.static('upload'));
app.use(express.static('img'));

app.use('/prive-wealth', routes);


module.exports = app